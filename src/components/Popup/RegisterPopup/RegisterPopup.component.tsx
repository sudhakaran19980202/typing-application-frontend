import { useState, useEffect, useCallback, Dispatch, SetStateAction } from "react"
import { useSelector, useDispatch } from "react-redux"
import { Modal, Input, Button, message } from "antd"
import styles from "@/components/Popup/RegisterPopup/RegisterPopup.module.css"
import useInput from "@/hooks/useInput";
import authApi from "@/redux/apis/authApi";
import profileApi from "@/redux/apis/profileApi";
import { registerUserData } from "@/interface/auth.interface";
import { RootState } from "@/redux/store/configureStore";
import { resetAuthState } from "@/redux/reducers/authSlice";

const { TextArea } = Input;

type Props = {
    openRegisterPopup: boolean,
    setOpenRegisterPopup: Dispatch<SetStateAction<boolean>>
}

const RegisterPopup = (props: Props) => {
    const { openRegisterPopup, setOpenRegisterPopup } = props

    const dispatch = useDispatch()
    const { registerUser } = authApi()
    const { getProfileDetails } = profileApi()
    const {
        registeringUser,
        registerUserSuccess,
        registerError
    } = useSelector((state: RootState) => state.auth)

    // ========================= USE STATES =========================
    // check password and confirm password mismatch
    const [passwordMismatch, setPasswordMismatch] = useState(false)

    // ========================= CUSTOM HOOKS =========================
    const { formData, handleChangeForm, resetForm } = useInput<registerUserData>({
        firstName: '', lastName: '', email: '', password: '', confirmPassword: '', bio: ''
    })

    // ========================= EVENT HANDLERS =========================

    const handleCheckPassword = useCallback(() => {
        if (formData?.password === formData?.confirmPassword) {
            setPasswordMismatch(false)
        } else {
            setPasswordMismatch(true)
        }
    }, [setPasswordMismatch, formData])

    const handleClosePopup = useCallback(() => {
        setOpenRegisterPopup(false)
        resetForm()
    }, [setOpenRegisterPopup, resetForm])

    const handleSubmitRegister = () => {
        registerUser(formData)
    }

    // ========================= USE EFFECTS =========================
    useEffect(() => {
        if (formData.confirmPassword) {
            handleCheckPassword()
        }
    }, [formData?.confirmPassword, handleCheckPassword])


    // ========================= API USE EFFECTS =========================
    useEffect(() => {
        if (registerError) {
            message.error(registerError)
        }
    }, [registerError])

    useEffect(() => {
        if (registerUserSuccess && !registeringUser) {
            message.success("Registered Successfully")

            dispatch(resetAuthState(['registerUserSuccess', 'registeringUser', 'registerError']))
            getProfileDetails()
            handleClosePopup()

        }
    }, [registerUserSuccess, registeringUser, getProfileDetails, handleClosePopup, dispatch])

    return (
        <Modal
            title="Register"
            className={styles.wrapper}
            open={openRegisterPopup}
            onCancel={handleClosePopup}
            footer={[
                <Button key="cancel-button" onClick={handleClosePopup}>Cancel</Button>,
                <Button key="register-button" loading={registeringUser} type="text" className={styles.registerButton} onClick={handleSubmitRegister}>Register</Button>
            ]}
        >
            <form className={styles.formWrapper}>
                <div className={styles.formItem}>
                    <label htmlFor="firstName">First Name: </label>
                    <Input
                        id="firstName"
                        name="firstName"
                        className={styles.formInput}
                        size={"large"}
                        value={formData.firstName}
                        onChange={handleChangeForm}
                    />
                </div>
                <div className={styles.formItem}>
                    <label htmlFor="lastName">Last Name: </label>
                    <Input
                        id="lastName"
                        name="lastName"
                        className={styles.formInput}
                        size={"large"}
                        value={formData.lastName}
                        onChange={handleChangeForm}
                    />
                </div>
                <div className={styles.formItem}>
                    <label htmlFor="email">Email: </label>
                    <Input
                        id="email"
                        name="email"
                        className={styles.formInput}
                        size={"large"}
                        value={formData.email}
                        onChange={handleChangeForm}
                    />
                </div>
                <div className={styles.formItem}>
                    <label htmlFor="password">Password: </label>
                    <Input
                        id="password"
                        name="password"
                        type="password"
                        className={styles.formInput}
                        size={"large"}
                        value={formData.password}
                        onChange={handleChangeForm}
                    />
                </div>
                <div className={styles.formItem}>
                    <label htmlFor="confirmPassword">Confirm Password: </label>
                    <Input
                        id="confirmPassword"
                        name="confirmPassword"
                        type="password"
                        className={styles.formInput}
                        size={"large"}
                        value={formData.confirmPassword}
                        onChange={handleChangeForm}
                    />
                    <span className={styles.mismatchText}>{passwordMismatch ? "Password mismatch" : null}</span>
                </div>
                <div className={styles.formItem} style={{ width: "100%" }}>
                    <label htmlFor="bio">Bio: </label>
                    <TextArea
                        id="bio"
                        name="bio"
                        className={styles.textArea}
                        size={"large"}
                        rows={6}
                        value={formData.bio}
                        onChange={handleChangeForm}
                    />
                </div>
            </form>
        </Modal>
    )
}

export default RegisterPopup