import { useState, useEffect, Dispatch, SetStateAction } from "react"
import { useSelector, useDispatch } from "react-redux"
import { Modal, Input, Button, message } from "antd"
import styles from "@/components/Popup/ResultPopup/ResultPopup.module.css"
import { RootState } from "@/redux/store/configureStore";
import { ScoreData } from "@/interface/score.interface";
import { resetScoreState } from "@/redux/reducers/scoreSlice";
import scoreApi from "@/redux/apis/scoreApi";


type Props = {
    openResultPopup: boolean,
    score: ScoreData,
    handleClosePopup: () => void,
}

const ResultPopup = (props: Props) => {
    const { openResultPopup, handleClosePopup, score } = props

    const dispatch = useDispatch()
    const { isAuthorized } = useSelector((state: RootState) => state.auth)
    const { createScoreSuccess, creatingScore, createScoreError } = useSelector((state: RootState) => state.scores)
    const { listScores, createScore } = scoreApi()

    const [category, setCategory] = useState({
        name: '', image: '', message: '', content: ''
    })

    /* eslint-disable react-hooks/exhaustive-deps */
    useEffect(() => {
        if (openResultPopup) {
            handleSetCategory()
        }
    }, [openResultPopup])

    useEffect(() => {
        if (createScoreError) {
            message.error(createScoreError)
            dispatch(resetScoreState(["createScoreError"]))
        }
    }, [createScoreError])

    useEffect(() => {
        if (createScoreSuccess && !creatingScore) {
            dispatch(resetScoreState(["createScoreSuccess", "creatingScore", "createScoreError"]))
            listScores()
        }
    }, [createScoreSuccess])
    /* eslint-enable react-hooks/exhaustive-deps */

    const handleSetCategory = () => {
        const categoryData = {
            name: '', image: '', message: '', content: ''
        }
        if (score.wpm < 25) {
            categoryData.name = "Sloth"
            categoryData.image = "images/sloth.jpg";
            categoryData.message = "Keep Practicing!";
            categoryData.content = "You're typing at a relaxed pace, prioritizing accuracy and speed.";

        } else if (score.wpm >= 25 && score.wpm < 40) {
            categoryData.name = "Turtle";
            categoryData.image = "images/turtle.png";
            categoryData.message = "Great Effort!"
            categoryData.content = "Your typing speed is moderate, and your accuracy is on point. Keep honing your skills, and you'll continue to improve.";
        } else if (score.wpm >= 40) {
            categoryData.name = "Rabbit";
            categoryData.image = "images/rabbit.jpg";
            categoryData.message = "Nice! Good Job!";
            categoryData.content = "Congratulations, you're typing at lightning speed with impressive accuracy. Keep up the fantastic work!"
        }
        setCategory(prevData => ({ ...prevData, ...categoryData }))

        // If the user is logged in, store the score values
        if (isAuthorized) {
            const scoreData = {
                wpm: score.wpm,
                cpm: score.cpm,
                accuracy: score.accuracy,
                type: categoryData.name
            }
            createScore(scoreData)
        }
    }

    return (
        <Modal
            className={styles.wrapper}
            open={openResultPopup}
            onCancel={handleClosePopup}
            footer={[]}
        >
            <div className={styles.resultWrapper}>
                {/* eslint-disable @next/next/no-img-element */}
                <img
                    className={styles.resultImage}
                    src={category?.image}
                    alt="Result"
                />
                {/* eslint-enable @next/next/no-img-element */}
                <div className={styles.resultContainer}>
                    <h1>You are a {category?.name}</h1>

                    <p className={styles.content}>
                        <span className={styles.contentMessage}>{category?.message}</span> You typed with the speed of {score?.wpm} WPM ({score?.cpm} CPM). Your accuracy was {score?.accuracy}%
                    </p>
                    <p style={{ color: "gray" }}>
                        {category?.content}
                    </p>
                </div>
            </div>
        </Modal>
    )
}

export default ResultPopup