import { useState, useEffect, Dispatch, SetStateAction } from "react"
import { useSelector, useDispatch } from "react-redux"
import { Modal, Input, Button, message } from "antd"
import styles from "@/components/Popup/LoginPopup/LoginPopup.module.css"
import useInput from "@/hooks/useInput";
import authApi from "@/redux/apis/authApi";
import profileApi from "@/redux/apis/profileApi";
import { loginUserData } from "@/interface/auth.interface";
import { RootState } from "@/redux/store/configureStore";
import { resetAuthState } from "@/redux/reducers/authSlice";

const { TextArea } = Input;

type Props = {
    openLoginPopup: boolean,
    setOpenLoginPopup: Dispatch<SetStateAction<boolean>>
}

const LoginPopup = (props: Props) => {
    const { openLoginPopup, setOpenLoginPopup } = props

    const dispatch = useDispatch()
    const { loginUser } = authApi()
    const { getProfileDetails } = profileApi()
    const {
        logginginUser,
        loginUserSuccess,
        loginError
    } = useSelector((state: RootState) => state.auth)


    // ========================= CUSTOM HOOKS =========================
    const { formData, handleChangeForm, resetForm } = useInput<loginUserData>({
        email: '', password: ''
    })

    // ========================= USE EFFECTS =========================

    /* eslint-disable react-hooks/exhaustive-deps */
    useEffect(() => {
        dispatch(resetAuthState(['loginError']))
    }, [])

    // ========================= API USE EFFECTS =========================
    useEffect(() => {
        if (loginError) {
            message.error(loginError)
            dispatch(resetAuthState(['loginError']))
        }
    }, [loginError])

    useEffect(() => {
        if (loginUserSuccess && !logginginUser) {
            message.success("Loggedin Successfully")

            dispatch(resetAuthState(['loginUserSuccess', 'logginginUser', 'loginError']))
            getProfileDetails()
            handleClosePopup()
        }
    }, [loginUserSuccess])
    /* eslint-enable react-hooks/exhaustive-deps */

    // ========================= EVENT HANDLERS =========================

    const handleClosePopup = () => {
        setOpenLoginPopup(false)
        resetForm()
    }

    const handleSubmitRegister = () => {
        loginUser(formData)
    }

    return (
        <Modal
            title="Login"
            className={styles.wrapper}
            open={openLoginPopup}
            onCancel={handleClosePopup}
            footer={[
                <Button key="cancel-button" onClick={handleClosePopup}>Cancel</Button>,
                <Button key="login-button" loading={logginginUser} type="text" className={styles.loginButton} onClick={handleSubmitRegister}>Login</Button>
            ]}
        >
            <form className={styles.formWrapper}>
                <div className={styles.formItem}>
                    <label htmlFor="email">Email: </label>
                    <Input
                        id="email"
                        name="email"
                        className={styles.formInput}
                        size={"large"}
                        value={formData.email}
                        onChange={handleChangeForm}
                    />
                </div>
                <div className={styles.formItem}>
                    <label htmlFor="password">Password: </label>
                    <Input
                        id="password"
                        name="password"
                        type="password"
                        className={styles.formInput}
                        size={"large"}
                        value={formData.password}
                        onChange={handleChangeForm}
                    />
                </div>
            </form>
        </Modal>
    )
}

export default LoginPopup