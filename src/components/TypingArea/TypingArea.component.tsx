import { useState, useRef, useEffect, Dispatch, SetStateAction } from "react"
import styles from './TypingArea.module.css';
import wordApi from "@/redux/apis/wordApi";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/redux/store/configureStore";
import { resetWordState } from "@/redux/reducers/wordSlice";
import { message, Tooltip } from "antd";


interface Score { wpm: number, cpm: number, accuracy: number, attemptWords: number }

type Props = {
    trigger: boolean,
    score: Score,
    openResultPopup: boolean,
    setTrigger: Dispatch<SetStateAction<boolean>>
    setScore: Dispatch<SetStateAction<Score>>
}

const TypingArea = (props: Props) => {
    const dispatch = useDispatch()
    const { openResultPopup, trigger, setTrigger, score, setScore } = props

    const { listWords } = wordApi()
    const { words, wordError, fetchWordsSuccess, fetchingWords } = useSelector((state: RootState) => state.words)

    const [remainingText, setRemainingText] = useState<string>('');
    const [validateText, setValidateText] = useState("")
    const [openTooltip, setOpenTooltip] = useState(false)
    const focusTypingText = useRef<HTMLInputElement | null>(null)

    // useEffect
    /* eslint-disable react-hooks/exhaustive-deps */
    useEffect(() => {
        listWords() // call list words api

        if (focusTypingText.current) {
            focusTypingText.current.focus()
        }
    }, [])

    useEffect(() => {
        setOpenTooltip(true)
        let interval = setTimeout(() => {
            setOpenTooltip(false)
        }, 3000)

        return () => {
            clearTimeout(interval)
        }
    }, [])

    useEffect(() => {
        if (openResultPopup) {
            handleResetWords()
        }
    }, [openResultPopup])

    useEffect(() => {
        if (wordError) {
            message.error("Something went wrong! please reload the page.")
            dispatch(resetWordState(['wordError']))
        }
    }, [wordError])
    useEffect(() => {
        if (fetchWordsSuccess && !fetchingWords) {
            dispatch(resetWordState(['fetchingWords', 'fetchWordsSuccess', 'wordError']))
            if (words) {
                setRemainingText(words?.join("\u00A0"))
            }
        }
    }, [fetchWordsSuccess])
    /* eslint-enable react-hooks/exhaustive-deps */

    const handleTypedText = (e: any) => {
        // triggering countdown
        handleTriggerCountdown()

        let text = e.currentTarget.textContent
        if (text) {
            const recentText = text[text.length - 1]
            if (recentText === remainingText[0]) {
                setRemainingText(remainingText.slice(1))
                setValidateText(validateText + recentText)
            }
        }
    }

    const handleKeyDown = (e: any) => {
        // triggering countdown
        // handleTriggerCountdown()

        const allowedKeys = /^[a-zA-Z ]$/;
        if (!e.key.match(allowedKeys)) {
            e.preventDefault();
        } else if (e.key === ' ') {
            const lastTypedText = e.currentTarget.textContent.split(" ").pop()
            const updatedAttemptWord = score.attemptWords + 1

            if (remainingText[0] === "\u00A0" && lastTypedText === validateText) {
                // calculate the score
                const updatedWpm = score.wpm + 1
                const updatedCpm = score.cpm + lastTypedText.length
                const updatedAccuracy = Math.round(updatedWpm / updatedAttemptWord * 100)
                setScore({
                    wpm: updatedWpm,
                    cpm: updatedCpm,
                    accuracy: updatedAccuracy,
                    attemptWords: updatedAttemptWord
                })
                setRemainingText(remainingText.slice(1))

            } else {
                let firstSpaceIndex = remainingText.indexOf("\u00A0")
                setRemainingText(remainingText.substring(firstSpaceIndex + 1))

                // Add an attempt word
                const updatedAccuracy = Math.round(score.wpm / updatedAttemptWord * 100)
                setScore({ ...score, attemptWords: updatedAttemptWord, accuracy: updatedAccuracy })
            }

            // Reset the validate text to find the next word
            setValidateText("")
        }
    };

    const handleTriggerCountdown = () => {
        if (!trigger) {
            setTrigger(true)
        }
    }

    const handleResetWords = () => {
        listWords()
        setValidateText("")
        if (focusTypingText.current) {
            focusTypingText.current.textContent = ""
        }
    }

    return (
        <section className={styles.typingArea}>

            <div className={styles.typingContainer}>
                <Tooltip title="Start Typing here" open={openTooltip} placement="topRight">
                    <span
                        ref={focusTypingText}
                        onInput={(e) => { handleTypedText(e) }}
                        onKeyDown={handleKeyDown}
                        contentEditable={true}
                        className={styles.typingText}
                    ></span>
                </Tooltip>
                <span
                    className={styles.remainingText}
                    onClick={() => { focusTypingText.current?.focus() }}
                >{remainingText}</span>

            </div>
            {/* <div style={{justifyContent: "flex-end", display: "flex", flexDirection: "row", width: "79%", paddingBottom: "10px"}}>
                <ExclamationCircleOutlined  /> instruction
            </div> */}
        </section>
    );
};

export default TypingArea