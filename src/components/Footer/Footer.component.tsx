import { useState, useEffect, ChangeEvent } from "react"
import styles from "@/components/Footer/Footer.module.css"
import subscriptionApi from "@/redux/apis/subscriptionApi"
import { RootState } from "@/redux/store/configureStore"
import { useDispatch, useSelector } from "react-redux"
import { resetSubscriptionState } from "@/redux/reducers/subscriptionSlice"
import { message, Button } from "antd"

type Props = {
    id: string
}
const Footer = (props: Props) => {
    const { id } = props;
    const dispatch = useDispatch()
    const { createSubscription, inviteUser } = subscriptionApi()
    const { createSubscriptionSuccess, 
        creatingSubscription, 
        createSubscriptionError,
        invitingUser,
        inviteUserSuccess,
        inviteUserError,
    } = useSelector((state: RootState) => state.subscriptions)

    const [email, setEmail] = useState("")
    const [isValid, setIsValid] = useState(false)

    const [inviteEmail, setInviteEmail] = useState("")
    const [isInviteMailValid, setIsInviteMailValid] = useState(false)


    /* eslint-disable react-hooks/exhaustive-deps */
    useEffect(() => {
        if (createSubscriptionError) {
            message.error(createSubscriptionError)
            dispatch(resetSubscriptionState(["createSubscriptionError"]))
        }
    }, [createSubscriptionError])

    useEffect(() => {
        if (createSubscriptionSuccess && !creatingSubscription) {
            dispatch(resetSubscriptionState(["createSubscriptionSuccess", "creatingSubscription", "createSubscriptionError"]))
            message.success("Thank you for subscribing!")

            setEmail("")
        }

    }, [createSubscriptionSuccess])

    useEffect(() => {
        if (inviteUserError) {
            message.error(inviteUserError)
            dispatch(resetSubscriptionState(["inviteUserError"]))
        }
    }, [inviteUserError])

    useEffect(() => {
        if (inviteUserSuccess && !invitingUser) {
            dispatch(resetSubscriptionState(["inviteUserSuccess", "invitingUser", "inviteUserError"]))
            message.success("Invitation has been sent successfully!")

            setInviteEmail("")
        }

    }, [inviteUserSuccess])
    /* eslint-enable react-hooks/exhaustive-deps */

    const validateEmail = (inputEmail: string, type: string) => {
        const emailPattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
        const isValidEmail = emailPattern.test(inputEmail);

        if (type === "SUBSCRIBE") {
            setIsValid(isValidEmail);
        } else {
            setIsInviteMailValid(isValidEmail)
        }
    };

    const handleEmailChange = (event: ChangeEvent<HTMLInputElement>) => {
        const inputEmail = event.target.value
        setEmail(inputEmail)
        validateEmail(inputEmail, "SUBSCRIBE")
    }

    const handleInviteEmailChange = (event: ChangeEvent<HTMLInputElement>) => {
        const inputEmail = event.target.value
        setInviteEmail(inputEmail)
        validateEmail(inputEmail, "INVITE")
    }

    const handleSubscription = () => {
        if (email && isValid) {
            return createSubscription({ email })
        }

        return message.warning("Please enter the valid email!")
    }

    const handleInvitation = () => {
        if (inviteEmail && isInviteMailValid) {
            return inviteUser({ email: inviteEmail })
        }

        return message.warning("Please enter the valid email!")
    }

    return (
        <footer id={id} className={styles.wrapper}>
            <section className={styles.footerSection}>
            {/* eslint-disable @next/next/no-img-element */}
                <img alt="footer-img" src={"images/footer-mail.svg"} className={styles.footerImage} />
                {/* eslint-enable @next/next/no-img-element */}
                <div className={styles.mailContact}>
                    <h1>Sign up Free</h1>
                    <p className={styles.content}>Boost your learning process and use the guides. Spend a few minutes on exploring them to get closer to your goal</p>
                    <input className={styles.mailInput} placeholder="Email Address" type="email" value={email} onChange={handleEmailChange}></input>
                    <Button className={styles.subscribeButton} onClick={handleSubscription}>Subscribe</Button>
                </div>
                <div className={styles.mailContact}>
                    <h1>Invite User</h1>
                    <p className={styles.inviteContent}>Send them an invitation to join our platform and enjoy the benefits together!</p>
                    <input className={styles.inviteInput} placeholder="Email Address" type="email" value={inviteEmail} onChange={handleInviteEmailChange}></input>
                    <Button loading={invitingUser} className={styles.inviteButton} onClick={handleInvitation}>Invite</Button>
                </div>
            </section>
        </footer>
    )
}

export default Footer