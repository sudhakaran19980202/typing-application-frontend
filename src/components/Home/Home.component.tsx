import { useState, useEffect } from "react"
import { Progress, Card, Input, Popover } from "antd"
import TypingArea from "@/components/TypingArea/TypingArea.component"
import ResultPopup from "../Popup/ResultPopup/ResultPopup.component";
import styles from "./Home.module.css"
import { ScoreData } from "@/interface/score.interface";
import { ExclamationCircleOutlined } from "@ant-design/icons"

type Props = {
    id: string
  }
const Home = (props: Props) => {
    const {id} = props
    const [percentage, setPercentage] = useState(100)
    const [openResultPopup, setOpenResultPopup] = useState(false)
    const [trigger, setTrigger] = useState(false)
    const [countdown, setCountdown] = useState(60)
    const [score, setScore] = useState<ScoreData>({
        wpm: 0,
        cpm: 0,
        accuracy: 0,
        attemptWords: 0
    })


    // Use effects
    /* eslint-disable react-hooks/exhaustive-deps */
    useEffect(() => {
        let interval: any;
        if (trigger) {
            handleCalculatePercentage()
            interval = setInterval(() => {
                if (countdown > 0) {
                    setCountdown(countdown - 1);
                } else {
                    handleResetTyping()
                    clearInterval(interval);
                }
            }, 1000);
        } else {
            setCountdown(60);
            clearInterval(interval);
        }

        return () => {
            clearInterval(interval);
        };
    }, [trigger, countdown]);
    /* eslint-enable react-hooks/exhaustive-deps */

    // Event Handlers
    const handleCalculatePercentage = () => {
        const seconds = 60;
        const calculatePercentage = (countdown / seconds) * 100;
        setPercentage(calculatePercentage)
    }

    const handleClosePopup = () => {
        setOpenResultPopup(false)
        setScore({ wpm: 0, cpm: 0, accuracy: 0, attemptWords: 0 }) // reset the score
    }

    const handleResetTyping = () => {
        setTrigger(false);
        setPercentage(100)
        setOpenResultPopup(true)
    }

    // UI function
    const instructionContent = () => {
        return (
            <div className={styles.instructionBox}>
                <h4>Instruction:</h4>
                <br />
                <ul>
                    <li>Only alphabet characters and the space bar are permitted for input. All other keys are disabled</li>
                    <li>If you type an incorrect character, you cannot backtrack or erase it. The incorrect character is considered a permanent error and cannot be corrected</li>
                    <li>To skip the current word and proceed to the next one, simply press the space bar</li>
                </ul>
            </div>
        )
    }

    return (
        <main id={id} className={styles.wrapper}>
            {/* Result Popup */}
            <ResultPopup openResultPopup={openResultPopup} score={score} handleClosePopup={handleClosePopup} />
            <section className={styles.contentSection}>
                <p className={styles.topTitle}>TYPING TEST SPEED</p>
                <h3 className={styles.mainTitle}>Test Your Typing Skills</h3>
            </section>
            <br />
            <br />
            <section className={styles.counterSection}>
                <div>
                    <Progress
                        type="circle"
                        percent={percentage}
                        strokeColor={{ '0%': '#ffd000', '100%': '#ffd000' }}
                        format={() => <p className={styles.progressContent}>{countdown} <span className={styles.progressSubContent}>Seconds</span></p>}
                    />
                </div>

                <div className={styles.counterSectionCard}>
                    <div className={styles.cardContainer}>
                        <Card className={styles.cardItem}>{score?.wpm}</Card>
                        <p>words/min</p>
                    </div>
                    <div className={styles.cardContainer}>
                        <Card className={styles.cardItem}>{score?.cpm}</Card>
                        <p>chars/min</p>
                    </div>
                    <div className={styles.cardContainer}>
                        <Card className={styles.cardItem}>{score?.accuracy}</Card>
                        <p>% accuracy</p>
                    </div>
                    <div className={styles.instructionIcon}>
                        <Popover content={instructionContent}>
                            <ExclamationCircleOutlined style={{ fontSize: "1em" }} />
                        </Popover>
                    </div>
                </div>
            </section>
            <br />
            <TypingArea trigger={trigger} openResultPopup={openResultPopup} setTrigger={setTrigger} score={score} setScore={setScore} />
        </main>
    )
}




export default Home