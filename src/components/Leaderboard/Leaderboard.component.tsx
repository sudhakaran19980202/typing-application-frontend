import { useEffect, useState } from "react"
import { Avatar } from "antd"
import styles from "@/components/Leaderboard/Leaderboard.module.css"
import leaderboardApi from "@/redux/apis/leaderboardApi"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/redux/store/configureStore"
import { resetLeaderboardState } from "@/redux/reducers/leaderboardSlice"

type Props = {
    id: string
}
const Leaderboard = (props: Props) => {
    const { id } = props
    const dispatch = useDispatch()

    const { listLeaderboards } = leaderboardApi()
    const { leaderboards, fetchLeaderboardsSuccess, fetchingLeaderboards, leaderboardError } = useSelector((state: RootState) => state.leaderboards)

    /* eslint-disable react-hooks/exhaustive-deps */
    useEffect(() => {
        listLeaderboards()
    }, [])

    useEffect(() => {
        if (fetchLeaderboardsSuccess && !fetchingLeaderboards) {
            dispatch(resetLeaderboardState(["fetchLeaderboardsSuccess", "fetchingLeaderboards"]))
        }
    }, [fetchLeaderboardsSuccess])
    /* eslint-enable react-hooks/exhaustive-deps */

    return (
        <div id={id} className={styles.wrapper}>
            <h1 className={styles.heading}>Leaderboard</h1>
            <section className={styles.leaderboard}>
                <div className={styles.tableTitle}>
                    <p>Profile</p>
                    <p>WPM (Words/Min)</p>
                    <p>CPM (Char/Min)</p>
                    <p>Accuracy %</p>
                </div>

                <div className={styles.tableTitleMob}>
                    <p>Profile</p>
                    <p>WPM</p>
                    <p>CPM</p>
                    <p>Accuracy</p>
                </div>
                {leaderboards?.map(item => {
                    return (
                        <div key={item._id} className={styles.leaderboardCard}>
                            <div className={styles.profile}>
                                <Avatar style={{ backgroundColor: '#00a2ae' }} >{item?.userId?.firstName[0]}</Avatar>
                                {item?.userId?.firstName} {item?.userId?.lastName}
                            </div>
                            <p>{item.wpm}</p>
                            <p>{item.cpm}</p>
                            <p>{item.accuracy}</p>
                        </div>
                    )
                })}
            </section>
        </div>
    )
}

export default Leaderboard