import { useEffect, useState } from "react"
import { Card, Progress, Button } from "antd"
import styles from "@/components/Score/Score.module.css"
import scoreApi from "@/redux/apis/scoreApi";
import RegisterPopup from "../Popup/RegisterPopup/RegisterPopup.component";
import LoginPopup from "../Popup/LoginPopup/LoginPopup.component";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/redux/store/configureStore";
import { resetScoreState } from "@/redux/reducers/scoreSlice";
import moment from "moment"

type Props = {
    id: string
}
const Score = (props: Props) => {
    const { id } = props

    const { isAuthorized } = useSelector((state: RootState) => state.auth)
    const { listScores } = scoreApi()

    /* eslint-disable react-hooks/exhaustive-deps */
    useEffect(() => {
        if (isAuthorized) {
            listScores()
        }
    }, [isAuthorized])
    /* eslint-enable react-hooks/exhaustive-deps */

    return (
        <div className={styles.wrapper} id={id}>
            <h1 className={styles.title}>Your Score</h1>
            {isAuthorized ? <ScoreSection /> : <NoScoreSection />}
        </div>
    )
}

// If the user is authorized, this score section will be rendered
const ScoreSection = () => {
    const { scores } = useSelector((state: RootState) => state.scores)
    return (
        <section className={styles.scoreContainer}>
            {scores?.map(item => {
                let cardColor = "#79e3d9"
                if (item.type === "Turtle") {
                    cardColor = "#fecc48"
                } else if (item.type === "Sloth") {
                    cardColor = "#ffb0c1"
                }
                return (
                    <>
                        <div key={item._id} className={`${styles.scoreCard}`}>
                            <div className={styles.topCard}>
                                <p className={styles.accuracy}>
                                    Accuracy
                                </p>
                                <p style={{ fontSize: "22px", color: cardColor, textTransform: "capitalize" }}>{item.type}</p>
                            </div>
                            <div className={styles.percentage}>{item.accuracy}%</div>
                            <div>
                                <Progress
                                    type="line"
                                    percent={item.accuracy}
                                    strokeColor={{ '0%': cardColor, '100%': cardColor }}
                                />
                            </div>
                            <div className={styles.endCard}>
                                <div className={styles.endSubCard}>
                                    <p>{item.wpm} wpm</p>
                                    <p>{item.cpm} cpm</p>
                                </div>
                                <p style={{ color: "gray" }}>{moment(item.createdAt).format("lll").toString()}</p>
                            </div>
                        </div>
                    </>
                )
            })}
        </section>
    )
}

// If the user is not loggedin/ not authorized, this no score section will be rendered
const NoScoreSection = () => {
    const [openLoginPopup, setOpenLoginPopup] = useState(false)
    const [openRegisterPopup, setOpenRegisterPopup] = useState(false)
    const handleRegisterButton = () => {
        setOpenRegisterPopup(true)
    }

    const handleLoginButton = () => {
        setOpenLoginPopup(true)
    }
    return (
        <div>
            {/* Register Popup */}
            <RegisterPopup openRegisterPopup={openRegisterPopup} setOpenRegisterPopup={setOpenRegisterPopup} />

            {/* Login Popup */}
            <LoginPopup openLoginPopup={openLoginPopup} setOpenLoginPopup={setOpenLoginPopup} />
            <div className={styles.buttonWrapper}>
                <p className={styles.noscoreText}>Please Signin / Register to track your score card</p>
                <Button type="text" className={styles.signinButton} onClick={handleLoginButton}>Signin</Button>
                <Button type="text" className={styles.registerButton} onClick={handleRegisterButton}>Register</Button>
            </div>
        </div>
    )
}

export default Score