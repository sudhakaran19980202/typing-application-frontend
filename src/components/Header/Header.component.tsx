import { useState, useEffect, MouseEvent } from "react"
import styles from "./Header.module.css"
import { SettingOutlined } from '@ant-design/icons';
import type { MenuProps, DrawerProps } from 'antd';
import { Row, Col, Button, Drawer, Avatar } from "antd"
import { MenuOutlined, HomeOutlined, RadarChartOutlined, BarChartOutlined, DeploymentUnitOutlined, PhoneOutlined } from "@ant-design/icons"
import RegisterPopup from "../Popup/RegisterPopup/RegisterPopup.component";
import LoginPopup from "../Popup/LoginPopup/LoginPopup.component";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/redux/store/configureStore";
import { resetAuthState } from "@/redux/reducers/authSlice";


const Header = () => {

  const dispatch = useDispatch()
  const { isAuthorized } = useSelector((state: RootState) => state.auth)
  const { user } = useSelector((state: RootState) => state.profiles)
  const [openNavigation, setOpenNavigation] = useState(false)
  const [openLoginPopup, setOpenLoginPopup] = useState(false)
  const [openRegisterPopup, setOpenRegisterPopup] = useState(false)

  const [placement, setPlacement] = useState<DrawerProps['placement']>('top');

  const navigationSection = [
    {
      name: "Home",
      icon: <HomeOutlined className={styles.drawerNavIcons} />,
      link: "#home"
    },
    {
      name: "Leaderboard",
      icon: <RadarChartOutlined className={styles.drawerNavIcons} />,
      link: "#leaderboard",
    },
    {
      name: "Score",
      icon: <BarChartOutlined className={styles.drawerNavIcons} />,
      link: "#score",
    },
    {
      name: "Contact",
      icon: <PhoneOutlined className={styles.drawerNavIcons} />,
      link: "#footer",
    },
  ]

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [])

  const handleNavigationClick = (event: MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    event.preventDefault();
    const targetId = event.currentTarget.getAttribute("href");
    if (targetId) {
      const targetElement = document.querySelector(targetId);

      if (targetElement instanceof HTMLElement) {
        window.scrollTo({
          top: targetElement.offsetTop,
          behavior: "smooth"
        });
      }
    }
  };

  const handleScroll = () => {
    const scrollPosition = window.scrollY;
    const sectionElements = Array.from(
      document.querySelectorAll("section[id]")
    );

    const activeSection = sectionElements.find((section) => {
      const adjustOffset = 0

      if (section instanceof HTMLElement) {
        const sectionTop = section.offsetTop - adjustOffset; // Adjust the offset as needed
        const sectionHeight = section.offsetHeight;
        return scrollPosition >= sectionTop && scrollPosition < sectionTop + sectionHeight;
      }

    });
  };

  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    dispatch(resetAuthState(['authError', 'profileError']))
  }, [])
  /* eslint-enable react-hooks/exhaustive-deps */

  const handleOpenNavigation = () => {
    setOpenNavigation(true)
  }

  const handleCloseNavigation = () => {
    setOpenNavigation(false)
  }

  const handleRegisterButton = () => {
    setOpenRegisterPopup(true)
  }

  const handleLoginButton = () => {
    setOpenLoginPopup(true)
  }

  return (
    <header className={styles.wrapper}>
      {/* Register Popup */}
      <RegisterPopup openRegisterPopup={openRegisterPopup} setOpenRegisterPopup={setOpenRegisterPopup} />

      {/* Login Popup */}
      <LoginPopup openLoginPopup={openLoginPopup} setOpenLoginPopup={setOpenLoginPopup} />

      <Drawer
        title=""
        placement={placement}
        closable={false}
        onClose={handleCloseNavigation}
        open={openNavigation}
        key={placement}
      >
        <section className={styles.drawerNavMenu}>
          {isAuthorized ? (
            <div className={styles.profileCard}>
              <Avatar style={{ backgroundColor: '#00a2ae' }} >{user?.firstName[0]}</Avatar>
              <span className={styles.profileName}>{user?.firstName} {user?.lastName}</span>
            </div>
          ) : null}
          {navigationSection?.map((item) => {
            return (
              <div key={item.name} className={styles.drawerNavSection}>
                {item.icon}
                <li>{item.name}</li>
              </div>
            )
          })}
        </section>
        <br />
        {!isAuthorized ? (
          <div className={styles.drawerNavButtons}>
            <Button className={styles.drawerNavLogin}>Login</Button>
            <Button type="text" className={styles.drawerNavSignup}>Sign up free</Button>
          </div>
        ) : null}

      </Drawer>
      <Row justify={"start"} align={"middle"}>
        <Col xs={24} md={1} />
        <Col xs={10} md={1}>
          {/* eslint-disable @next/next/no-img-element */}
          <img alt="logo" src={"/images/logo.jpeg"} className={styles.logo} />
          {/* eslint-enable @next/next/no-img-element */}
        </Col>
        <Col xs={10} md={4}>
          <h2>TYPING APP!</h2>
        </Col>
        <Col xs={0} md={14}>
          <nav className={styles.menu}>
            {navigationSection?.map(item => {
              return (
                <a
                  key={item.name}
                  href={item.link}
                  onClick={() => handleNavigationClick}
                >{item.name}</a>
              )
            })}
          </nav>
        </Col>
        {isAuthorized ? (
          <Col xs={0} md={4}>
            <div className={styles.profileCard}>
              <Avatar style={{ backgroundColor: '#00a2ae' }} >{user?.firstName[0]}</Avatar>
              <span className={styles.profileName}>{user?.firstName} {user?.lastName}</span>
            </div>
          </Col>
        ) : (
          <Col xs={0} md={4}>
            <>
              <Button type="text" className={styles.signinButton} onClick={handleLoginButton}>Signin</Button>
              <Button type="text" className={styles.registerButton} onClick={handleRegisterButton}>Register</Button>
            </>
          </Col>
        )}

        <Col xs={2} />
        <Col xs={2} md={0}>
          <MenuOutlined onClick={handleOpenNavigation} />
        </Col>
      </Row>
    </header>
  )
}

export default Header