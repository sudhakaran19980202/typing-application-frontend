import { Provider } from "react-redux"
import { Layout } from "antd"
import { PersistGate } from "redux-persist/integration/react"

import Header from "@/components/Header/Header.component"
import Main from "@/components/Home/Home.component"
import Leaderboard from "@/components/Leaderboard/Leaderboard.component"
import Score from "@/components/Score/Score.component"
import Footer from "@/components/Footer/Footer.component"
import store from "@/redux/store/configureStore"
import { persistor } from "@/redux/store/configureStore"

export default function Home() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Layout>
          <Header/>
          <Main  id="home" />
          <Leaderboard id="leaderboard" />
          <Score id="score" />
          <Footer id="footer" />
        </Layout>
      </PersistGate>
    </Provider>
  )
}
