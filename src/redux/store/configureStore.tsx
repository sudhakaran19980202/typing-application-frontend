import { configureStore } from '@reduxjs/toolkit';
import {persistStore, persistReducer} from "redux-persist"
import storage from 'redux-persist/lib/storage';
import rootReducer from './rootReducer';

// persist storage
const persistConfig = {
  key: "root",
  storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)
const store = configureStore({
  reducer: persistedReducer,
});

export default store
export const persistor = persistStore(store)
export type RootState = ReturnType<typeof store.getState>