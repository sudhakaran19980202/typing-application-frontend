import { combineReducers } from "@reduxjs/toolkit";
import authReducer from "../reducers/authSlice";
import profileReducer from "../reducers/profileSlice";
import wordReducer from "../reducers/wordSlice";
import scoreReducer from "../reducers/scoreSlice";
import leaderboardReducer from "../reducers/leaderboardSlice";
import subscriptionReducer from "../reducers/subscriptionSlice";

const rootReducer = combineReducers({
    auth: authReducer,
    profiles: profileReducer,
    words: wordReducer,
    scores: scoreReducer,
    leaderboards: leaderboardReducer,
    subscriptions: subscriptionReducer
})

export default rootReducer