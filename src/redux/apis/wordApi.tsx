import axios, { AxiosError } from "axios"
import { useDispatch } from "react-redux";
import {
    fetchWordsRequest,
    fetchWordsSuccess,
    fetchWordsFailure,
} from "@/redux/reducers/wordSlice"
import { config } from "@/config/config";

function WordApi() {
    // declarations
    const dispatch = useDispatch()
    const API_URL = config.API_URL
    const TOKEN = typeof window !== "undefined" ? localStorage.getItem("token") : null

    // list words api
    const listWords = async () => {
        dispatch(fetchWordsRequest())

        try {
            const apiResponse = await axios({
                method: 'GET',
                baseURL: API_URL,
                url: '/common/words/list',
                headers: {
                    Authorization: TOKEN
                }
            })

            dispatch(fetchWordsSuccess(apiResponse?.data?.data))
        } catch (error) {
            if (axios.isAxiosError(error)) {
                const errorMessage = error?.response?.data?.message
                dispatch(fetchWordsFailure(errorMessage))
            } else {
                dispatch(fetchWordsFailure("Server Error"))
            }
        }

    }

    return { listWords}
}

export default WordApi