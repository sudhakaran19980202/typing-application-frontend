import axios, { AxiosError } from "axios"
import { useDispatch } from "react-redux";
import {
    fetchLeaderboardsRequest,
    fetchLeaderboardsSuccess,
    fetchLeaderboardsFailure,
} from "@/redux/reducers/leaderboardSlice"
import { config } from "@/config/config";

function LeaderboardApi() {
    // declarations
    const dispatch = useDispatch()
    const API_URL = config.API_URL
    const TOKEN = typeof window !== "undefined" ? localStorage.getItem("token") : null

    // list leaderboards api
    const listLeaderboards = async () => {
        dispatch(fetchLeaderboardsRequest())

        try {
            const apiResponse = await axios({
                method: 'GET',
                baseURL: API_URL,
                url: '/common/leaderboards',
                headers: {
                    Authorization: TOKEN
                }
            })

            dispatch(fetchLeaderboardsSuccess(apiResponse?.data?.data))
        } catch (error) {
            if (axios.isAxiosError(error)) {
                const errorMessage = error?.response?.data?.message
                dispatch(fetchLeaderboardsFailure(errorMessage))
            } else {
                dispatch(fetchLeaderboardsFailure("Server Error"))
            }
        }

    }

    return { listLeaderboards}
}

export default LeaderboardApi