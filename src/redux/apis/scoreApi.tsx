import axios, { AxiosError } from "axios"
import { useDispatch } from "react-redux";
import {
    fetchScoresRequest,
    fetchScoresSuccess,
    fetchScoresFailure,

    createScoreRequest,
    createScoreSuccess,
    createScoreFailure,
} from "@/redux/reducers/scoreSlice"
import { reqBodyScoreData } from "@/interface/score.interface";
import { config } from "@/config/config";

function ScoreApi() {
    // declarations
    const dispatch = useDispatch()
    const API_URL = config.API_URL
    const TOKEN = typeof window !== "undefined" ? localStorage.getItem("token") : null

    // list scores
    const listScores = async () => {
        dispatch(fetchScoresRequest())

        try {
            const apiResponse = await axios({
                method: 'GET',
                baseURL: API_URL,
                url: '/user/scores',
                headers: {
                    Authorization: TOKEN
                }
            })

            dispatch(fetchScoresSuccess(apiResponse?.data?.data))
        } catch (error) {
            if (axios.isAxiosError(error)) {
                const errorMessage = error?.response?.data?.message
                dispatch(fetchScoresFailure(errorMessage))
            } else {
                dispatch(fetchScoresFailure("Server Error"))
            }
        }

    }

    // create score
    const createScore = async (data: reqBodyScoreData) => {
        dispatch(createScoreRequest())

        try {
            await axios({
                method: 'POST',
                baseURL: API_URL,
                url: '/user/scores',
                headers: {
                    Authorization: TOKEN
                },
                data
            })

            dispatch(createScoreSuccess())
        } catch (error) {
            if (axios.isAxiosError(error)) {
                const statusCode = error?.response?.status
                let errorMessage = error?.response?.data?.message
                if (statusCode === 401) {
                    errorMessage = "Unauthorized"
                }
                dispatch(createScoreFailure(errorMessage))
            } else {
                dispatch(createScoreFailure("Server Error"))
            }
        }
    }

    return { listScores, createScore }
}

export default ScoreApi