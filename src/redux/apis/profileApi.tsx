import axios, { AxiosError } from "axios"
import { useDispatch } from "react-redux";
import {
    fetchUserRequest,
    fetchUserSuccess,
    fetchUserFailure,


} from "@/redux/reducers/profileSlice"
import { registerUserData } from "@/interface/auth.interface";
import { config } from "@/config/config";

function ProfileApi() {
    // declarations
    const dispatch = useDispatch()
    const API_URL = config.API_URL
    const TOKEN = typeof window !== "undefined" ? localStorage.getItem("token") : null

    // get profile details
    const getProfileDetails = async () => {
        dispatch(fetchUserRequest())

        try {
            const apiResponse = await axios({
                method: 'GET',
                baseURL: API_URL,
                url: '/user/profile-details',
                headers: {
                    Authorization: TOKEN
                }
            })

            dispatch(fetchUserSuccess(apiResponse?.data?.data))
        } catch (error) {
            if (axios.isAxiosError(error)) {
                const errorMessage = error?.response?.data?.message
                dispatch(fetchUserFailure(errorMessage))
            } else {
                dispatch(fetchUserFailure("Server Error"))
            }
        }

    }

    return { getProfileDetails}
}

export default ProfileApi