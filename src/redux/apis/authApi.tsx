import axios, { AxiosError } from "axios"
import { useDispatch } from "react-redux";
import {
    registerUserRequest,
    registerUserSuccess,
    registerUserFailure,

    loginUserRequest,
    loginUserSuccess,
    loginUserFailure,

    logoutUserRequest,
    logoutUserSuccess,

} from "@/redux/reducers/authSlice"
import { loginUserData, registerUserData } from "@/interface/auth.interface";
import { config } from "@/config/config";

function AuthApi() {
    // declarations
    const dispatch = useDispatch()
    const API_URL = config.API_URL

    // register api
    const registerUser = async (data: registerUserData) => {
        dispatch(registerUserRequest())

        try {
            const apiResponse = await axios({
                method: 'POST',
                baseURL: API_URL,
                url: '/auth/signup',
                data
            })

            dispatch(registerUserSuccess())
            localStorage.setItem("token", `jwt ${apiResponse?.data?.token}`)
        } catch (error) {
            if (axios.isAxiosError(error)) {
                const errorMessage = error?.response?.data?.message
                dispatch(registerUserFailure(errorMessage))
            } else {
                dispatch(registerUserFailure("Server Error"))
            }
        }
    }

    // login api
    const loginUser = async (data: loginUserData) => {
        dispatch(loginUserRequest())

        try {
            const apiResponse = await axios({
                method: 'POST',
                baseURL: API_URL,
                url: '/auth/login',
                data
            })

            dispatch(loginUserSuccess())
            localStorage.setItem("token", `jwt ${apiResponse?.data?.token}`)
        } catch (error) {
            if (axios.isAxiosError(error)) {
                const statusCode = error?.response?.status
                let errorMessage = error?.response?.data?.message
                if (statusCode === 401) {
                    errorMessage = "Invalid email or password"
                }
                dispatch(loginUserFailure(errorMessage))
            } else {
                dispatch(loginUserFailure("Server Error"))
            }
        }
    }

    return { registerUser, loginUser }
}

export default AuthApi