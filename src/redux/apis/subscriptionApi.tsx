import axios, { AxiosError } from "axios"
import { useDispatch } from "react-redux";
import {
    createSubscriptionRequest,
    createSubscriptionSuccess,
    createSubscriptionFailure,

    inviteUserRequest,
    inviteUserSuccess,
    inviteUserFailure,
} from "@/redux/reducers/subscriptionSlice"
import { config } from "@/config/config";
import { reqBodySubscriptionData } from "@/interface/subscription.interface";


function SubscriptionApi() {
    // declarations
    const dispatch = useDispatch()
    const API_URL = config.API_URL

    // create subscription
    const createSubscription = async (data: reqBodySubscriptionData) => {
        dispatch(createSubscriptionRequest())

        try {
            await axios({
                method: 'POST',
                baseURL: API_URL,
                url: '/common/subscriptions',
                data
            })

            dispatch(createSubscriptionSuccess())
        } catch (error) {
            if (axios.isAxiosError(error)) {
                let errorMessage = error?.response?.data?.message
                dispatch(createSubscriptionFailure(errorMessage))
            } else {
                dispatch(createSubscriptionFailure("Server Error"))
            }
        }
    }

    // invite user
    const inviteUser = async (data: reqBodySubscriptionData) => {
        dispatch(inviteUserRequest())

        try {
            await axios({
                method: 'POST',
                baseURL: API_URL,
                url: '/common/invite-user',
                data
            })

            dispatch(inviteUserSuccess())
        } catch (error) {
            if (axios.isAxiosError(error)) {
                const statusCode = error?.response?.status
                let errorMessage = error?.response?.data?.message
                dispatch(inviteUserFailure(errorMessage))
            } else {
                dispatch(inviteUserFailure("Server Error"))
            }
        }
    }

    return { createSubscription, inviteUser }
}

export default SubscriptionApi