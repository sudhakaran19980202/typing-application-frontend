import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { ApiScoreData } from "@/interface/score.interface"

interface Score {
    // list scores
    fetchingScores: boolean,
    fetchScoresSuccess: boolean,
    scores: ApiScoreData[] | null,
    listScoreError: string,

    // create a score
    creatingScore: boolean,
    createScoreSuccess: boolean,
    createScoreError: string,
}

const initialState: Score = {
    // list scores
    fetchingScores: false,
    fetchScoresSuccess: false,
    scores: null,
    listScoreError: "",

    // create a score
    creatingScore: false,
    createScoreSuccess: false,
    createScoreError: ""
}

const scoreSlice = createSlice({
    name: "scores",
    initialState,
    reducers: {
        // reset state reducer
        resetScoreState: (state, action: PayloadAction<string[]>) => {
            const resetFields = action.payload
            resetFields.forEach((field) => {
                if (initialState.hasOwnProperty(field)) {
                    (state as Record<keyof Score, any>)[field as keyof Score] = initialState[field as keyof Score]
                }
            })
        },

        // list scores reducer
        fetchScoresRequest: (state) => {
            state.fetchingScores = true;
        },
        fetchScoresSuccess: (state, action) => {
            state.fetchingScores = false;
            state.fetchScoresSuccess = true;
            state.scores = action.payload;
            state.listScoreError = ""
        },
        fetchScoresFailure: (state, action) => {
            state.fetchingScores = false;
            state.fetchScoresSuccess = false;
            state.scores = null;
            state.listScoreError = action.payload
        },

        // create score reducer
        createScoreRequest: (state) => {
            state.creatingScore = true;
        },
        createScoreSuccess: (state) => {
            state.creatingScore = false;
            state.createScoreSuccess = true;
            state.createScoreError = ""
        },
        createScoreFailure: (state, action) => {
            state.creatingScore = false;
            state.createScoreSuccess = false;
            state.createScoreError = action.payload
        }
    }
})

export const {
    resetScoreState,

    fetchScoresRequest,
    fetchScoresSuccess,
    fetchScoresFailure,

    createScoreRequest,
    createScoreSuccess,
    createScoreFailure,

} = scoreSlice.actions
export default scoreSlice.reducer;