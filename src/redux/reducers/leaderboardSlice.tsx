import { ApiLeaderboardData } from "@/interface/leaderboard.interface"
import { createSlice, PayloadAction } from "@reduxjs/toolkit"

interface Leaderboard {
    // list score details
    fetchingLeaderboards: boolean,
    fetchLeaderboardsSuccess: boolean,
    leaderboards: ApiLeaderboardData[] | null,

    leaderboardError: string,
}

const initialState: Leaderboard = {

    // list leaderboard details
    fetchingLeaderboards: false,
    fetchLeaderboardsSuccess: false,
    leaderboards: null,

    leaderboardError: "",

}

const leaderboardSlice = createSlice({
    name: "leaderboards",
    initialState,
    reducers: {
        // reset state reducer
        resetLeaderboardState: (state, action: PayloadAction<string[]>) => {
            const resetFields = action.payload
            resetFields.forEach((field) => {
                if (initialState.hasOwnProperty(field)) {
                    (state as Record<keyof Leaderboard, any>)[field as keyof Leaderboard] = initialState[field as keyof Leaderboard]
                }
            })
        },

        // list leaderboards reducer
        fetchLeaderboardsRequest: (state) => {
            state.fetchingLeaderboards = true;
        },
        fetchLeaderboardsSuccess: (state, action) => {
            state.fetchingLeaderboards = false;
            state.fetchLeaderboardsSuccess = true;
            state.leaderboards = action.payload;
            state.leaderboardError = ""
        },
        fetchLeaderboardsFailure: (state, action) => {
            state.fetchingLeaderboards = false;
            state.fetchLeaderboardsSuccess = false;
            state.leaderboards = null;
            state.leaderboardError = action.payload
        },
    }
})

export const {
    resetLeaderboardState,

    fetchLeaderboardsRequest,
    fetchLeaderboardsSuccess,
    fetchLeaderboardsFailure,

} = leaderboardSlice.actions
export default leaderboardSlice.reducer;