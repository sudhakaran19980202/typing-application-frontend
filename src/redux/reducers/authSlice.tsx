import { createSlice, PayloadAction } from "@reduxjs/toolkit"

interface Auth {
    isAuthorized: boolean,

    // register state type
    registeringUser: boolean,
    registerUserSuccess: boolean,
    registerError: string,

    // login state type
    logginginUser: boolean,
    loginUserSuccess: boolean,
    loginError: string,

    // logout state type
    loggingoutUser: boolean,
    logoutUserSuccess: boolean
}

const initialState: Auth = {
    isAuthorized: false,

    // register state
    registeringUser: false,
    registerUserSuccess: false,
    registerError: '',

    // login state
    logginginUser: false,
    loginUserSuccess: false,
    loginError: '',

    // logout state
    loggingoutUser: false,
    logoutUserSuccess: false

}

const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        // reset state reducer
        resetAuthState: (state, action: PayloadAction<string[]>) => {
            const resetFields = action.payload
            resetFields.forEach((field) => {
                if (initialState.hasOwnProperty(field)) {
                    (state as Record<keyof Auth, any>)[field as keyof Auth] = initialState[field as keyof Auth]
                }
            })
        },

        // user register reducer
        registerUserRequest: (state) => {
            state.registeringUser = true;
        },
        registerUserSuccess: (state) => {
            state.registeringUser = false;
            state.registerUserSuccess = true
            state.isAuthorized = true;
            state.registerError = ""
        },
        registerUserFailure: (state, action) => {
            state.registeringUser = false;
            state.registerUserSuccess = false
            state.registerError = action.payload
        },

        // user login reducer
        loginUserRequest: (state) => {
            state.logginginUser = true
        },
        loginUserSuccess: (state) => {
            state.logginginUser = false;
            state.loginUserSuccess = true
            state.isAuthorized = true;
            state.loginError = ""
        },
        loginUserFailure: (state, action) => {
            state.logginginUser = false;
            state.loginUserSuccess = false;
            state.isAuthorized = false;
            state.loginError = action.payload;
        },

        // user logout reducer
        logoutUserRequest: (state) => {
            state.loggingoutUser = true
        },
        logoutUserSuccess: (state) => {
            state.loggingoutUser = false;
            state.isAuthorized = false
            state.logoutUserSuccess = true
        }

    }
})

export const {
    resetAuthState,

    registerUserRequest,
    registerUserSuccess,
    registerUserFailure,

    loginUserRequest,
    loginUserSuccess,
    loginUserFailure,

    logoutUserRequest,
    logoutUserSuccess,
} = authSlice.actions
export default authSlice.reducer;