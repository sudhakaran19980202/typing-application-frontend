import { createSlice, PayloadAction } from "@reduxjs/toolkit"

interface Subscription {
    // create subscription
    creatingSubscription: boolean,
    createSubscriptionSuccess: boolean,
    createSubscriptionError: string,

    // invite User
    invitingUser: boolean,
    inviteUserSuccess: boolean,
    inviteUserError: string
}

const initialState: Subscription = {
    // create a score
    creatingSubscription: false,
    createSubscriptionSuccess: false,
    createSubscriptionError: "",

    // invite user
    invitingUser: false,
    inviteUserSuccess: false,
    inviteUserError: "",
}

const subscriptionSlice = createSlice({
    name: "subscriptions",
    initialState,
    reducers: {
        // reset state reducer
        resetSubscriptionState: (state, action: PayloadAction<string[]>) => {
            const resetFields = action.payload
            resetFields.forEach((field) => {
                if (initialState.hasOwnProperty(field)) {
                    (state as Record<keyof Subscription, any>)[field as keyof Subscription] = initialState[field as keyof Subscription]
                }
            })
        },

        // create subscription reducer
        createSubscriptionRequest: (state) => {
            state.creatingSubscription = true;
        },
        createSubscriptionSuccess: (state) => {
            state.creatingSubscription = false;
            state.createSubscriptionSuccess = true;
            state.createSubscriptionError = ""
        },
        createSubscriptionFailure: (state, action) => {
            state.creatingSubscription = false;
            state.createSubscriptionSuccess = false;
            state.createSubscriptionError = action.payload
        },

        // invite user reducer
        inviteUserRequest: (state) => {
            state.invitingUser = true;
        },
        inviteUserSuccess: (state) => {
            state.invitingUser = false;
            state.inviteUserSuccess = true;
            state.inviteUserError = ""
        },
        inviteUserFailure: (state, action) => {
            state.invitingUser = false;
            state.inviteUserSuccess = false;
            state.inviteUserError = action.payload
        }
    }
})

export const {
    resetSubscriptionState,

    createSubscriptionRequest,
    createSubscriptionSuccess,
    createSubscriptionFailure,

    inviteUserRequest,
    inviteUserSuccess,
    inviteUserFailure,

} = subscriptionSlice.actions
export default subscriptionSlice.reducer;