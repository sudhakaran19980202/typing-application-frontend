import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { ApiUserData } from "@/interface/profile.interface"

interface Profile {
    // common state type
    profileError: string,

    // get profile details
    fetchingUser: boolean,
    fetchUserSuccess: boolean,
    user: ApiUserData | null,
}

const initialState: Profile = {
    // auth common state
    profileError: "",

    // get profile details
    fetchingUser: false,
    fetchUserSuccess: false,
    user: null,
}

const profileSlice = createSlice({
    name: "profiles",
    initialState,
    reducers: {
        // reset state reducer
        resetProfileState: (state, action: PayloadAction<string[]>) => {
            const resetFields = action.payload
            resetFields.forEach((field) => {
                if (initialState.hasOwnProperty(field)) {
                    (state as Record<keyof Profile, any>)[field as keyof Profile] = initialState[field as keyof Profile]
                }
            })
        },

        // get user details reducer
        fetchUserRequest: (state) => {
            state.fetchingUser = true;
        },
        fetchUserSuccess: (state, action) => {
            state.fetchingUser = false;
            state.fetchUserSuccess = true;
            state.user = action.payload;
            state.profileError = ""
        },
        fetchUserFailure: (state, action) => {
            state.fetchingUser = false;
            state.fetchUserSuccess = false;
            state.user = null;
            state.profileError = action.payload
        },
    }
})

export const {
    resetProfileState,

    fetchUserRequest,
    fetchUserSuccess,
    fetchUserFailure,

} = profileSlice.actions
export default profileSlice.reducer;