import { createSlice, PayloadAction } from "@reduxjs/toolkit"

interface Word {
    // list score details
    fetchingWords: boolean,
    fetchWordsSuccess: boolean,
    words: string[] | null,

    wordError: string,
}

const initialState: Word = {

    // list words
    fetchingWords: false,
    fetchWordsSuccess: false,
    words: null,

    wordError: "",

}

const wordSlice = createSlice({
    name: "words",
    initialState,
    reducers: {
        // reset state reducer
        resetWordState: (state, action: PayloadAction<string[]>) => {
            const resetFields = action.payload
            resetFields.forEach((field) => {
                if (initialState.hasOwnProperty(field)) {
                    (state as Record<keyof Word, any>)[field as keyof Word] = initialState[field as keyof Word]
                }
            })
        },

        // list words reducer
        fetchWordsRequest: (state) => {
            state.fetchingWords = true;
        },
        fetchWordsSuccess: (state, action) => {
            state.fetchingWords = false;
            state.fetchWordsSuccess = true;
            state.words = action.payload;
            state.wordError = ""
        },
        fetchWordsFailure: (state, action) => {
            state.fetchingWords = false;
            state.fetchWordsSuccess = false;
            state.words = null;
            state.wordError = action.payload
        },
    }
})

export const {
    resetWordState,

    fetchWordsRequest,
    fetchWordsSuccess,
    fetchWordsFailure,

} = wordSlice.actions
export default wordSlice.reducer;