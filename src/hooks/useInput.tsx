import {useState, ChangeEvent} from "react"
import { registerUserData, loginUserData } from "@/interface/auth.interface"

type FormReturnType<T> = {
    formData: T;
    handleChangeForm: (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
    resetForm: () => void;
}

function useInput<T extends registerUserData | loginUserData>(initialState: T): FormReturnType<T> {
    const [formData, setFormData] = useState(initialState)

    const handleChangeForm = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const {name, value} = event.target
        setFormData({...formData, [name]: value})
    }

    const resetForm = () => {
        setFormData(initialState)
    }

    return {formData, handleChangeForm, resetForm}
}

export default useInput