export interface ApiUserData {
    _id: string,
    firstName: string,
    lastName: string,
    email: string,
    bio: string
}