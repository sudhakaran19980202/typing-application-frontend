// Components
export interface ScoreData {
    wpm: number,
    cpm: number,
    accuracy: number,
    attemptWords: number
}


// Reducers
export interface ApiScoreData {
    _id: string,
    userId: string,
    wpm: number,
    cpm: number,
    accuracy: number,
    type: string,
    createdAt: string,
    updatedAt: string
}

// Api Request Body
export interface reqBodyScoreData {
    wpm: number,
    cpm: number,
    accuracy: number,
}