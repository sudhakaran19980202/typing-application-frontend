export interface ApiLeaderboardData {
    _id: string,
    wpm: number,
    cpm: number,
    accuracy: number,
    userId: {
        firstName: string,
        lastName: string
    }
}