export interface registerUserData {
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    confirmPassword: string,
    bio: string
}

export interface loginUserData {
    email: string,
    password: string
}
